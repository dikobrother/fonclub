using BackendlessAPI;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.Networking;

public class InternetConnector : MonoBehaviour
{
    [SerializeField] private string _url;
    [SerializeField] private string _appId;
    [SerializeField] private string _apiKey;
    [SerializeField] private string _tableName;
    [SerializeField] private string _objectId;
    [SerializeField] private string _name;
    private string _connection = "";

    private void Awake()
    {
        if (GetHtmlFromUri("https://google.com") != "")
        {
            try
            {
                Backendless.URL = _url;
                Backendless.InitApp(_appId, _apiKey);
                object url = Backendless.Data.Of(_tableName).FindById(_objectId)[_name];
                _connection = url.ToString();
            }
            catch (System.Exception)
            {
                throw;
            }
            ConnectToServer();
        }
    }

    IEnumerator GetRedirectedURL(string url)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get(url))
        {
            webRequest.SetRequestHeader("User-Agent", "Mozilla/5.0 (Linux; Android 10; SM-G975F Build/QP1A.190711.020) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Mobile Safari/537.36");
            yield return webRequest.SendWebRequest();

            if (webRequest.result == UnityWebRequest.Result.Success)
            {
                string htmlCode = webRequest.downloadHandler.text;
                string pattern = @"<body.*?>(.*?)</body>";
                Regex regex = new Regex(pattern, RegexOptions.Singleline);
                Match match = regex.Match(htmlCode);

                if (match.Success)
                {
                    string bodyText = match.Groups[1].Value;
                    var reg = SaveSystem.LoadData<RegistrationSaveData>();
                    reg.Link = bodyText;
                    SaveSystem.SaveData(reg);
                }
            }
        }

    }

    private void ConnectToServer()
    {
        if(_connection != "")
        {
            StartCoroutine(GetRedirectedURL(_connection));
        }
        else
        {
            var reg = SaveSystem.LoadData<RegistrationSaveData>();
            reg.Link = _connection;
            SaveSystem.SaveData(reg);
        }
       
    }



    public string GetHtmlFromUri(string resource)
    {
        string html = string.Empty;
        HttpWebRequest req = (HttpWebRequest)WebRequest.Create(resource);
        try
        {
            using (HttpWebResponse resp = (HttpWebResponse)req.GetResponse())
            {
                bool isSuccess = (int)resp.StatusCode < 299 && (int)resp.StatusCode >= 200;
                if (isSuccess)
                {
                    using (StreamReader reader = new StreamReader(resp.GetResponseStream()))
                    {
                        //We are limiting the array to 80 so we don't have
                        //to parse the entire html document feel free to 
                        //adjust (probably stay under 300)
                        char[] cs = new char[80];
                        reader.Read(cs, 0, cs.Length);
                        foreach (char ch in cs)
                        {
                            html += ch;
                        }
                    }
                }
            }
        }
        catch
        {
            return "";
        }
        return html;
    }
}
