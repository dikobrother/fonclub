using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "QuestionsBase", menuName = "QuestionsBase", order = 1)]
public class QuestionsBase : ScriptableObject
{
    public List<string> Questions;
    public List<string> Answers;
}

