using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class StartLevelButton : MonoBehaviour
{
    [SerializeField] private TMP_Text _levelText;
    [SerializeField] private Image _levelImage;
    [SerializeField] private Sprite _enableSprite;
    [SerializeField] private Sprite _disableSprite;
    [SerializeField] private Button _button;

    public void Init(bool enabled, int level)
    {
        _levelText.text = "������� " + level;
        if (enabled)
        {
            _levelImage.sprite = _enableSprite;
            _button.onClick.AddListener(() => OnLevelButtonClicked(level));
        }
        else
        {
            _levelImage.sprite = _disableSprite;
        }
    }

    private void OnLevelButtonClicked(int level)
    {
        var levelSave = SaveSystem.LoadData<LevelSaveData>();
        levelSave.CurrentLevel = level;
        SaveSystem.SaveData(levelSave);
        SceneLoader.LoadScene("GameScene");
    }
}
