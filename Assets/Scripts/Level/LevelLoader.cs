using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public class LevelLoader : MonoBehaviour
{
    [SerializeField] private Transform _lettersParent;
    [SerializeField] private Transform _answerParent;
    [SerializeField] private LetterButton _letterPrefab;
    [SerializeField] private LetterField _answerPrefab;
    [SerializeField] private QuestionsBase _questionsBase;
    [SerializeField] private List<LetterButton> _letterButtons;
    [SerializeField] private List<LetterField> _letterFields;
    [SerializeField] private TaskController _taskController;
    [SerializeField] private TMP_Text _questionText;
    [SerializeField] private TMP_Text _levelText;
    private List<char> _answerChars = new List<char>();

    private void Awake()
    {
        _questionText.text = _questionsBase.Questions[SaveSystem.LoadData<LevelSaveData>().CurrentLevel - 1];
        _levelText.text = SaveSystem.LoadData<LevelSaveData>().CurrentLevel + " �������";
        char[] chars = _questionsBase.Answers[SaveSystem.LoadData<LevelSaveData>().CurrentLevel - 1].ToCharArray();
        foreach (var item in chars)
        {
            _answerChars.Add(item);
        }
        CreateLetterFields();
        if (_questionsBase.Answers[SaveSystem.LoadData<LevelSaveData>().CurrentLevel - 1].Length >= 8)
        {
            for (int i = 0; i < 18; i++)
            {
                CreateLetterButton();
            }
        }
        else
        {
            for (int i = 0; i < 12; i++)
            {
                CreateLetterButton();
            }
        }
        _taskController.Init(_letterButtons, _letterFields, _questionsBase.Answers[SaveSystem.LoadData<LevelSaveData>().CurrentLevel - 1]);
        SetLetterToButtons();
        
    }

    private void CreateLetterFields()
    {
        for (int i = 0; i < _answerChars.Count; i++)
        {
            LetterField newLetterField = Instantiate(_answerPrefab, _answerParent);
            _letterFields.Add(newLetterField);
        }
        _letterFields[0].SetLetter('_');
    }

    private void CreateLetterButton()
    {
        LetterButton newLetter = Instantiate(_letterPrefab, _lettersParent);
        _letterButtons.Add(newLetter);
    }

    private void SetLetterToButtons()
    {
        List<LetterButton> buttons = new List<LetterButton>();
        foreach (var item in _letterButtons)
        {
            buttons.Add(item);
        }
        for (int i = 0; i < _answerChars.Count; i++)
        {
            int random = Random.Range(0, buttons.Count);
            buttons[random].Init(_answerChars[i]);
            buttons.Remove(buttons[random]);
        }
        char[] arr = Enumerable.Range(0, 32).Select((x, i) => (char)('�' + i)).ToArray();
        foreach (var item in buttons)
        {
            item.Init(arr[Random.Range(0, arr.Length)]);
        }
        buttons.Clear();
    }
}
