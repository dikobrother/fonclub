using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class LoaderBar : MonoBehaviour
{
    [SerializeField] private Image _progressBar;
    [SerializeField] private TMP_Text _percentageText;
    [SerializeField] private GameObject _canvas;

    private void Start()
    {
        _progressBar.DOFillAmount(1, 2f).OnUpdate(() => SetPercentageText()).OnComplete(() => 
        {
            _canvas.SetActive(true);
            gameObject.SetActive(false);
        });
    }

    private void SetPercentageText()
    {
        _percentageText.text = (_progressBar.fillAmount * 100).ToString("#") + "%";
    }
}
