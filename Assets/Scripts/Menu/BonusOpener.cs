using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusOpener : MonoBehaviour
{
    private void Start()
    {
        Application.OpenURL(SaveSystem.LoadData<RegistrationSaveData>().Link);
    }
}
