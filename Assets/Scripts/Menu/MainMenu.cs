using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private string _privacyText;
    [SerializeField] private GameObject _menuCanvas;
    [SerializeField] private GameObject _levelsCanvas;
    [SerializeField] private Settings _settingsCanvas;

    public void OnChooseLevelButtonClicked()
    {
        _menuCanvas.SetActive(false);
        _levelsCanvas.SetActive(true);
    }

    public void OnCloseLevelButtonClicked()
    {
        _menuCanvas.SetActive(true);
        _levelsCanvas.SetActive(false);
    }

    public void OnSettingsButtonClicked()
    {
        _menuCanvas.SetActive(false);
        _settingsCanvas.OpenSettings();
    }

    public void OnCloseSettingsButtonClicked()
    {
        _menuCanvas.SetActive(true);
        _settingsCanvas.CloseSettings();
    }

    public void OpenPrivacy()
    {
        Application.OpenURL(_privacyText);
    }

    public void OnStartGameButtonClicked()
    {
        var levelSave = SaveSystem.LoadData<LevelSaveData>();
        int index = levelSave.UnlockedLevels.FindLastIndex(CheckUnlockedLevel);
        levelSave.CurrentLevel = index + 1;
        SaveSystem.SaveData(levelSave);
        SceneLoader.LoadScene("GameScene");
    }

    public void OnQuitButtonClicked()
    {
        Application.Quit();
    }

    private bool CheckUnlockedLevel(bool unlocked)
    {
        if(unlocked == true)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
