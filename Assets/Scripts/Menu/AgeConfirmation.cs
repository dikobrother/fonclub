using UnityEngine;
using UnityEngine.SceneManagement;

public class AgeConfirmation : MonoBehaviour
{
    public void OnYesButtonClicked()
    {
        var reg = SaveSystem.LoadData<RegistrationSaveData>();
        reg.Registered = true;
        SaveSystem.SaveData(reg);
        if (reg.Link.Contains("docs.google") || reg.Link == "")
        {
            SceneManager.LoadScene("MenuScene");
        }
        else
        {
            SceneManager.LoadScene("BonusScene");
        }
    }

}
