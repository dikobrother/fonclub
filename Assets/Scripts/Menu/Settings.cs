using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;
using UnityEngine.UI;

public class Settings : MonoBehaviour
{
    [SerializeField] private AudioController _musicContoller;
    [SerializeField] private Image _musicImage;
    [SerializeField] private Image _soundImage;
    [SerializeField] private Image _musicIcon;
    [SerializeField] private Image _soundIcon;
    [SerializeField] private Sprite _onButton;
    [SerializeField] private Sprite _offButton;
    [SerializeField] private Sprite _muteIcon;
    [SerializeField] private Sprite _unmuteIcon;

    private void Awake()
    {
        UpdateSettings();
    }

    public void UpdateSettings()
    {
        var settings = SaveSystem.LoadData<SettingSaveData>();
        if (settings.IsMusicOn)
        {
            _musicImage.sprite = _onButton;
            _musicIcon.sprite = _unmuteIcon;
        }
        else
        {
            _musicImage.sprite = _offButton;
            _musicIcon.sprite = _muteIcon;
        }
        if (settings.IsSoundOn)
        {
            _soundImage.sprite = _onButton;
            _soundIcon.sprite = _unmuteIcon;
        }
        else
        {
            _soundImage.sprite = _offButton;
            _soundIcon.sprite = _muteIcon;
        }
    }

    public void OpenSettings()
    {
        UpdateSettings();
        gameObject.SetActive(true);
    }

    public void CloseSettings()
    {
        gameObject.SetActive(false);
    }

    public void OnMusicButtonClicked()
    {
        var settings = SaveSystem.LoadData<SettingSaveData>();
        if (settings.IsMusicOn)
        {
            settings.IsMusicOn = false;
        }
        else
        {
            settings.IsMusicOn = true;
        }
        SaveSystem.SaveData(settings);
        _musicContoller.UpdateAudio();
        UpdateSettings();
    }

    public void OnSoundButtonClicked()
    {
        var settings = SaveSystem.LoadData<SettingSaveData>();
        if (settings.IsSoundOn)
        {
            settings.IsSoundOn = false;
        }
        else
        {
            settings.IsSoundOn = true;
        }
        SaveSystem.SaveData(settings);
        _musicContoller.UpdateAudio();
        UpdateSettings();
    }

}
