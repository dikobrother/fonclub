using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelsUpdater : MonoBehaviour
{
    [SerializeField] private Transform _buttonsParent;
    [SerializeField] private StartLevelButton _startLevelButtonPrefab;

    private void OnEnable()
    {
        UpdateLevels();
    }

    private void UpdateLevels()
    {
        for (int i = 0; i < SaveSystem.LoadData<LevelSaveData>().UnlockedLevels.Count; i++)
        {
            StartLevelButton newButton = Instantiate(_startLevelButtonPrefab, _buttonsParent);
            newButton.Init(SaveSystem.LoadData<LevelSaveData>().UnlockedLevels[i], i + 1);
        }
    }
}
