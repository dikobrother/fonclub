using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioController : MonoBehaviour
{
    [SerializeField] private AudioMixer _audioMixer;

    private float _minVolume = -80f;
    private float _maxVolume = 0f;

    private void Start()
    {
        UpdateAudio();
    }

    public void UpdateAudio()
    {
        var settings = SaveSystem.LoadData<SettingSaveData>();
        if (settings.IsMusicOn)
        {
            TurnOnMusic();
        }
        else
        {
            TurnOffMusic();
        }
        if (settings.IsSoundOn)
        {
            TurnOnSound();
        }
        else
        {
            TurnOffSound();
        }
    }

    public void TurnOffMusic()
    {
        _audioMixer.SetFloat("MusicVolume", _minVolume);
    }

    public void TurnOnMusic()
    {
        _audioMixer.SetFloat("MusicVolume", _maxVolume);
    }

    public void TurnOffSound()
    {
        _audioMixer.SetFloat("SoundVolume", _minVolume);
    }

    public void TurnOnSound()
    {
        _audioMixer.SetFloat("SoundVolume", _maxVolume);
    }

}
