using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TaskController : MonoBehaviour
{
    [SerializeField] private GameResult _gameResult;
    [SerializeField] private GameObject _openLetterPanel;
    [SerializeField] private GameObject _skipLevelPanel;
    [SerializeField] private int _openLetterCost;
    [SerializeField] private int _skipLevelCost;
    [SerializeField] private AudioSource _soundSource;
    private List<LetterButton> _letterButtons = new List<LetterButton>();
    private List<LetterField> _letterFields = new List<LetterField>();
    private string _correctAnswer;

    public void Init(List<LetterButton> letterButtons, List<LetterField> letterFields, string answer)
    {
        foreach (var item in letterButtons)
        {
            _letterButtons.Add(item);
            item.LetterButtonClicked += OnLetterButtonClicked;
        }
        foreach (var item in letterFields)
        {
            _letterFields.Add(item);
        }
        _correctAnswer = answer;
    }

    public void OnLetterButtonClicked(char letter, LetterButton button)
    {
        _soundSource.Play();
        LetterField currentLetterField = GetCurrentLetterField();
        if(currentLetterField == null)
        {
            return;
        }
        button.SetFieldIndex(_letterFields.IndexOf(currentLetterField));
        currentLetterField.SetLetter(letter);
        button.LetterButtonClicked -= OnLetterButtonClicked;
        button.UnsetButtonClicked += OnUnsetButtonClicked;
        if(GetCurrentLetterField() != null)
        {
            GetCurrentLetterField().SetLetter('_');
        }
        else
        {
            TaskCompleted();
        }
    }

    public void OnUnsetButtonClicked(LetterButton button)
    {
        _soundSource.Play();
        GetCurrentLetterField().ClearField();
        _letterFields[button.GetFieldIndex()].ClearField();
        button.SetFieldIndex(_correctAnswer.Length + 1);
        button.LetterButtonClicked += OnLetterButtonClicked;
        button.UnsetButtonClicked -= OnUnsetButtonClicked;
        GetCurrentLetterField().SetLetter('_');
    }

    public void OpenUnlockLetterPanel()
    {
        _openLetterPanel.SetActive(true);
    }

    public void CloseUnlockLetterPanel()
    {
        _openLetterPanel.SetActive(false);
    }

    public void OpenSkipLevelPanel()
    {
        _skipLevelPanel.SetActive(true);
    }

    public void CloseSkipLevelPanel()
    {
        _skipLevelPanel.SetActive(false);
    }

    public void SkipLevel()
    {
        if (Wallet.CanRemoveMoney(_skipLevelCost))
        {
            Wallet.RemoveMoney(_skipLevelCost);
            var level = SaveSystem.LoadData<LevelSaveData>();
            if (level.UnlockedLevels.Count > level.CurrentLevel)
            {
                level.UnlockedLevels[level.CurrentLevel] = true;
                level.CurrentLevel = level.CurrentLevel + 1;
                SaveSystem.SaveData(level);
            }
            SceneLoader.LoadScene("GameScene");
        }
    }
    
    public void OpenOneLetter()
    {
        if (Wallet.CanRemoveMoney(_openLetterCost))
        {
            Wallet.RemoveMoney(_openLetterCost);
            LetterField currentField = GetCurrentLetterField();
            char[] chars = _correctAnswer.ToCharArray();
            currentField.SetLetter(chars[_letterFields.IndexOf(currentField)]);
            if (GetCurrentLetterField() == null)
            {
                TaskCompleted();
            }
            CloseUnlockLetterPanel();
        }
    }

    private void TaskCompleted()
    {
        string userAnswer = "";
        foreach (var item in _letterFields)
        {
            userAnswer += item.GetCurrentLetter();
        }
        if (userAnswer.ToUpper() == _correctAnswer.ToUpper())
        {
            _gameResult.Win();
        }
        else
        {
            _gameResult.Lose();
        }
    }

    public LetterField GetCurrentLetterField()
    {
        foreach (var item in _letterFields)
        {
            if (!item.CheckHasLetter())
            {
                return item;
            }
        }
        return null;
    }
}
