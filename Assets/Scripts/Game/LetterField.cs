using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LetterField : MonoBehaviour
{
    [SerializeField] private char _letter;
    [SerializeField] private TMP_Text _letterText;
    private bool _hasLetter = false;
    public void SetLetter(char letter)
    {
        _letter = letter;
        _letterText.text = _letter.ToString();
        if(_letter != '_')
        {
            _hasLetter = true;
        }
    }

    public void ClearField()
    {
        _hasLetter = false;
        _letter = ' ';
        _letterText.text = _letter.ToString();
    }

    public bool CheckHasLetter()
    {
        return _hasLetter;
    }

    public char GetCurrentLetter()
    {
        return _letter;
    }


}
