using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameResult : MonoBehaviour
{
    [SerializeField] private GameObject _winPanel;
    [SerializeField] private GameObject _losePanel;

    public void Win()
    {
        _winPanel.SetActive(true);
        Wallet.AddMoney(25);
        var level = SaveSystem.LoadData<LevelSaveData>();
        if(level.UnlockedLevels.Count > level.CurrentLevel)
        {
            level.UnlockedLevels[level.CurrentLevel] = true;
            level.CurrentLevel = level.CurrentLevel + 1;
            SaveSystem.SaveData(level);
        }
    }

    public void Lose()
    {
        _losePanel.SetActive(true);
    }

    public void RestartGame()
    {
        SceneLoader.LoadScene("GameScene");
    }

    public void GoToMenu()
    {
        SceneLoader.LoadScene("MenuScene");
    }

}
