using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LetterButton : MonoBehaviour
{
    [SerializeField] private TMP_Text _letterText;
    [SerializeField] private Button _letterButton;
    [SerializeField] private Image _backgroundImage;
    [SerializeField] private Color _setColor;
    [SerializeField] private Color _unsetColor;
    [SerializeField] private Color _setTextColor;
    [SerializeField] private Color _unsetTextColor;
    [SerializeField] private char _currentLetter;
    private int _fieldIndex;
    private bool _isHasLetter = false;
    public Action<char, LetterButton> LetterButtonClicked;
    public Action<LetterButton> UnsetButtonClicked;

    public void Init(char letter)
    {
        _currentLetter = letter;
        _letterText.text = _currentLetter.ToString();
        _isHasLetter = true;
        _letterButton.onClick.AddListener(OnLetterButtonClicked);
    }

    public void OnLetterButtonClicked()
    {
        LetterButtonClicked?.Invoke(_currentLetter, this);
        _backgroundImage.color = _unsetColor;
        _letterText.color = _unsetTextColor;
        _letterButton.onClick.RemoveAllListeners();
        _letterButton.onClick.AddListener(OnUnsetButtonClicked);
    }

    public void OnUnsetButtonClicked()
    {
        UnsetButtonClicked?.Invoke(this);
        _backgroundImage.color = _setColor;
        _letterText.color = _setTextColor;
        _letterButton.onClick.RemoveAllListeners();
        _letterButton.onClick.AddListener(OnLetterButtonClicked);
    }

    public void SetFieldIndex(int index)
    {
        _fieldIndex = index;
    }

    public int GetFieldIndex()
    {
        return _fieldIndex;
    }

    public bool CheckHasLetter()
    {
        return _isHasLetter;
    }


}
