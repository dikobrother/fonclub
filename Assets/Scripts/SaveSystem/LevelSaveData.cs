using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class LevelSaveData : SaveData
{
    public int CurrentLevel { get; set; }
    public List<bool> UnlockedLevels { get; set; }

    public LevelSaveData(int currentLevel, List<bool> unlockedLevels)
    {
        CurrentLevel = currentLevel;
        UnlockedLevels = unlockedLevels;
    }
}
