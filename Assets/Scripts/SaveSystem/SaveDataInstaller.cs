using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SaveDataInstaller : MonoBehaviour
{
    [SerializeField] private bool _fromTheBeginning;
    [SerializeField] private int _startMoney;
    [SerializeField] private int _levelsCount;

    private void Start()
    {
        InstallBindings();
    }

    private void InstallBindings()
    {
        BindFileNames();
        BindMoney();
        BindLevel();
        BindRegistration();
        BindSettings();
        LoadScene();
    }

    private void LoadScene()
    {
        Wallet.SetStartMoney();
        var reg = SaveSystem.LoadData<RegistrationSaveData>();
        if (!reg.Registered)
        {
            SceneManager.LoadScene("LoadingScene");
            return;
        }
        if(reg.Link != "")
        {
            if (reg.Link.Contains("docs.google"))
            {
                SceneManager.LoadScene("MainMenu");
            }
            else
            {
                SceneManager.LoadScene("BonusScene");
            }
        }
        else
        {
            if (reg.Registered)
            {
                SceneManager.LoadScene("MainMenu");
            }
        }

        

    }

    private void BindRegistration()
    {
        {
            var reg = SaveSystem.LoadData<RegistrationSaveData>();

#if UNITY_EDITOR
            if (_fromTheBeginning)
            {
                reg = null;
            }
#endif

            if (reg == null)
            {
                reg = new RegistrationSaveData("", false);
                SaveSystem.SaveData(reg);
            }

        }
    }

    private void BindMoney()
    {
        {
            var money = SaveSystem.LoadData<MoneySaveData>();

        #if UNITY_EDITOR
            if (_fromTheBeginning)
            {
                money = null;
            }
        #endif

            if (money == null)
            {
                money = new MoneySaveData(_startMoney);
                SaveSystem.SaveData(money);
            }

        }
    }

    private void BindLevel()
    {
        {
            var levels = SaveSystem.LoadData<LevelSaveData>();

#if UNITY_EDITOR
            if (_fromTheBeginning)
            {
                levels = null;
            }
#endif

            if (levels == null)
            {
                List<bool> lvls = new List<bool>();
                for (int i = 0; i < _levelsCount; i++)
                {
                    lvls.Add(false);
                }
                lvls[0] = true;
                levels = new LevelSaveData(1, lvls);
                SaveSystem.SaveData(levels);
            }

        }
    }

    private void BindSettings()
    {
        {
            var settings = SaveSystem.LoadData<SettingSaveData>();

#if UNITY_EDITOR
            if (_fromTheBeginning)
            {
                settings = null;
            }
#endif

            if (settings == null)
            {
                settings = new SettingSaveData(true, true);
                SaveSystem.SaveData(settings);
            }

        }
    }

    private void BindFileNames()
    {
        FileNamesContainer.Add(typeof(MoneySaveData), FileNames.MoneyData);
        FileNamesContainer.Add(typeof(LevelSaveData), FileNames.LevelData);
        FileNamesContainer.Add(typeof(RegistrationSaveData), FileNames.RegData);
        FileNamesContainer.Add(typeof(SettingSaveData), FileNames.SettingsData);
    }

}